export const Images = {
  img_settings: require('./img/settings.png'),
  img_avatar_big: require('./img/avatar_big.png'),
  img_edit: require('./img/edit.png'),
  img_chocolates: require('./img/chocolates.png'),
  img_desserts: require('./img/desserts.png'),
  img_egg: require('./img/egg.png'),
  img_italian: require('./img/italian.png'),
  img_noodle: require('./img/noodle.png'),
  img_sweets: require('./img/sweets.png'),
  img_search: require('./img/search.png'),
  img_filter: require('./img/filter.png'),
  img_cardamom: require('./img/cardamom.png'),
  img_coconut: require('./img/coconut.png'),
  img_banana: require('./img/banana.png'),
  img_blanched: require('./img/blanched.png'),
  img_sauteed: require('./img/sauteed.png'),
  img_tenderized: require('./img/tenderized.png'),
  img_search_active: require('./img/search_active.png'),
  img_search_unactive: require('./img/search_unactive.png'),
  img_recipe_active: require('./img/recipe_active.png'),
  img_recipe: require('./img/recipe.png'),
  img_back: require('./img/back.png'),
  img_logo: require('./img/logo.png'),
  img_notification: require('./img/notification.png'),
  img_message: require('./img/message.png'),
  img_is_liked: require('./img/is_liked.png'),
  img_is_unLiked: require('./img/is_unLiked.png'),
  img_add: require('./img/add.png'),
  img_avatar: require('./img/avatar.png'),
  img_cured_vegetables: require('./img/cured_vegetables.png'),
  img_red_wine: require('./img/red_wine.png'),
  img_vanilla_pud: require('./img/vanilla_pud.png'),
  img_white_wine: require('./img/white_wine.png'),
  img_feed: require('./img/feed.png'),
  img_feed_active: require('./img/feed_active.png'),
  img_background: require('./img/background.png'),
  img_cover: require('./img/cover.png'),
  img_edit_green: require('./img/edit_green.png'),
  img_gallery1: require('./img/gallery1.png'),
  img_gallery2: require('./img/gallery2.png'),
  img_gallery3: require('./img/gallery3.png'),
  img_gallery4: require('./img/gallery4.png'),
  img_close: require('./img/close.png'),
  img_cover_gallery1: require('./img/cover_gallery1.png'),
  img_cover_gallery2: require('./img/cover_gallery2.png'),
  img_radio_button: require('./img/radio_button.png'),
  img_remove: require('./img/remove.png'),
  img_add_gray: require('./img/add_gray.png'),
};
