interface Colors {
  primary: string,
  gray: string
}

const colors: Colors = {
  primary: '#30BE76',
  gray: 'gray'
};

export default colors;
