import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  ImageSourcePropType,
} from 'react-native';

interface Recipe {
  id: string;
  title: string;
  image: ImageSourcePropType;
}

interface Props {
  data: Recipe;
  index: number;
}

export const RecipeItem: React.FC<Props> = ({data, index}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={[
        styles.item,
        // eslint-disable-next-line react-native/no-inline-styles
        index % 2 === 0 ? {marginRight: 5} : {marginLeft: 5},
      ]}>
      <Image source={data.image} style={styles.image} />
      <View style={styles.titleWrapper}>
        <Text>{data.title}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  item: {
    width: 150,
    height: 132,
    alignItems: 'center',
    borderRadius: 8,
    overflow: 'hidden',
    flex: 1,
    backgroundColor: 'white',
  },
  image: {
    width: 180,
    height: 100,
  },
  titleWrapper: {
    justifyContent: 'center',
    height: 30,
  },
});
