import React, {useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import {Images} from '../../images';
// import {SCREEN} from '../../routes/Screen';
import {Colors} from '../../themes';
import {useDispatch, useSelector, RootStateOrAny} from 'react-redux';
import {authActions} from '../../store/actions/authActions';
import {SCREEN} from '../../routes/Screen';

export const LoginScreen = props => {
  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const pending: boolean = useSelector(
    (state: RootStateOrAny) => state.auth.loginPending,
  );
  const dispatch = useDispatch();

  const onLoginHandle = () => {
    const data = {userName, password};
    dispatch(
      authActions.login(data, () => props.navigation.navigate(SCREEN.TAB)),
    );
  };

  return (
    <View style={styles.container}>
      <ActivityIndicator animating={pending} size={24} color={'red'} />
      <ImageBackground
        source={Images.img_background}
        style={styles.backgorundImage}>
        <Image source={Images.img_logo} />
        <Text style={{fontSize: 25, fontWeight: 'bold'}}>Welcome Back!</Text>
        <View style={{height: 24}} />
      </ImageBackground>
      <View style={styles.content}>
        <Text>Please login to continue.</Text>
        <View>
          <TextInput
            value={userName}
            style={styles.textInput}
            onChangeText={text => setUserName(text)}
          />
          <View style={styles.passwordWrapper}>
            <Text style={styles.passwordText}>Password</Text>
            <TouchableOpacity>
              <Text>Forgot password?</Text>
            </TouchableOpacity>
          </View>
          <TextInput
            value={password}
            onChangeText={text => setPassword(text)}
            secureTextEntry={true}
            style={styles.textInput}
          />
          <TouchableOpacity style={styles.loginButton} onPress={() => onLoginHandle()}>
            <Text style={{color: 'white', fontWeight: 'bold'}}>Login</Text>
          </TouchableOpacity>
          <View style={{alignItems: 'center', marginTop: 30}}>
            <Text>Start to Scratch</Text>
            <TouchableOpacity>
              <Text style={{color: Colors.primary, fontWeight: 'bold'}}>
                Crate Account Here
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{height: 24}} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backgorundImage: {
    width: Dimensions.get('window').width,
    height: 285,
    padding: 20,
    justifyContent: 'space-around',
  },
  passwordText: {
    color: 'gray',
  },
  passwordWrapper: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginTop: 30,
  },
  content: {
    padding: 20,
    justifyContent: 'space-between',
    flex: 1,
  },
  loginButton: {
    backgroundColor: '#30BE76',
    marginTop: 30,
    borderRadius: 8,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInput: {
    borderBottomWidth: 1,
    height: 40,
  },
});
