import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  FlatList,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {Images} from '../../images';
import {
  trendingRecipesData,
  recipesCookPotatoData,
  recipesCookBananaData,
  recipesTomatoData,
  recipesPumpkinData,
} from '../../constants';
import {ListItem} from './components';

export const SearchScreen = () => {
  const [trendingRecipes, setTrendingrecipe] = useState(trendingRecipesData);
  const [recipesCook, setRecipesCook] = useState(recipesCookPotatoData);
  const [selected, setSelected] = useState(0);
  const menuBar = ['Potato', 'Banana', 'Tomato', 'Pumpkin'];

  const onSelectedHandle = (index: number) => {
    setSelected(index);

    if (selected === 0) {
      setRecipesCook(recipesCookPotatoData);
    } else if (selected === 1) {
      setRecipesCook(recipesCookBananaData);
    } else if (selected === 2) {
      setRecipesCook(recipesTomatoData);
    } else {
      setRecipesCook(recipesPumpkinData);
    }
  };
  return (
    <ScrollView>
      <View style={{flex: 1, padding: 10}}>
        <View style={styles.textInputWrapper}>
          <Image source={Images.img_search} />
          <TextInput style={{width: 300}} />
          <Image source={Images.img_filter} />
        </View>
        <Text style={{fontWeight: 'bold', marginTop: 30}}>
          Trending Recipes
        </Text>
        <View style={{marginTop: 20}}>
          <FlatList
            keyExtractor={item => item.id}
            ItemSeparatorComponent={() => <View style={{width: 10}} />}
            showsHorizontalScrollIndicator={false}
            horizontal
            data={trendingRecipes}
            renderItem={({item}) => <ListItem data={item} />}
          />
        </View>
        <View style={styles.line} />
        <Text style={{fontWeight: 'bold', marginTop: 15}}>
          What can I make with...
        </Text>
        <View style={{marginTop: 10}}>
          <ScrollView showsHorizontalScrollIndicator={false} horizontal>
            {menuBar.map((menu, index) => (
              <TouchableOpacity
                key={menu}
                activeOpacity={0.8}
                onPress={() => onSelectedHandle(index)}>
                <Text
                  style={[
                    styles.menuText,
                    selected === index && {color: 'black'},
                  ]}>
                  {menu}
                </Text>
              </TouchableOpacity>
            ))}
          </ScrollView>
        </View>
        <View style={{marginTop: 15}}>
          <FlatList
            keyExtractor={item => item.id}
            ItemSeparatorComponent={() => <View style={{width: 10}} />}
            showsHorizontalScrollIndicator={false}
            horizontal
            data={recipesCook}
            renderItem={({item}) => <ListItem data={item} />}
          />
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  menuText: {
    fontSize: 18,
    fontWeight: 'bold',
    marginRight: 30,
    color: '#ccc',
  },
  textInputWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    padding: 8,
    borderRadius: 8,
    borderColor: '#ccc',
    shadowColor: 'black',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 6,
    elevation: 8,
    shadowOpacity: 0.26,
  },
  line: {
    height: 1,
    backgroundColor: '#ccc',
    marginTop: 40,
  },
});
