import React from 'react';
import {View, Text, Image, ImageSourcePropType} from 'react-native';

interface RecipesCook {
  id: string,
  title: string,
  image: ImageSourcePropType
}

interface Props {
  data: RecipesCook
}

export const ListItem: React.FC<Props> = ({data}) => {
  return (
    <View style={{width: 120}}>
      <Image source={data.image} />
      <Text style={{marginTop: 10}}>{data.title}</Text>
    </View>
  );
};
