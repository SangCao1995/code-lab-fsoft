import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  Alert,
  PixelRatio,
  PermissionsAndroid,
  Platform,
} from 'react-native';
import {Header} from '../../components';
import {Colors} from '../../themes';
import * as EmailValidator from 'email-validator';
import {Helpers, UserProfileStorage} from '../../utils';
import {SCREEN} from '../../routes/Screen';
import {Scale} from '../../utils';
import {launchImageLibrary} from 'react-native-image-picker';
import {WrapComponent} from '../../hoc/wrapComponentWithFlex';
import {RouteProp} from '@react-navigation/native';
import {NavigationScreenProp} from 'react-navigation';
import {UserProfile} from '../../constants';

type ProfileEditingParamsList = {
  PROFILE_EDITING_SCREEN: {
    userProfile: UserProfile;
    // onSelect: () => void;
  };
};

type ProfileEditingRouteProps = RouteProp<
  ProfileEditingParamsList,
  'PROFILE_EDITING_SCREEN'
>;

type ProfileEditingNavigationProps = NavigationScreenProp<{}>;

interface ProfileEditingProps {
  route: ProfileEditingRouteProps;
  navigation: ProfileEditingNavigationProps;
}

export const ProfileEdittingScreen: React.FC<ProfileEditingProps> = props => {
  const userProfile = props.route.params.userProfile;
  const [fullName, setFullName] = useState(userProfile.fullName);
  const [bio, setBio] = useState(userProfile.bio);
  const [email, setEmail] = useState(userProfile.email);
  const [phone, setPhone] = useState(userProfile.phone);
  const [avatar, setAvatar] = useState<string | undefined>(userProfile.avatar);

  const onSaveProfileHandle = () => {
    if (!EmailValidator.validate(email)) {
      Alert.alert('Invalid email', 'Please check again your email!', [
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ]);
    } else if (!Helpers.validatePhone(phone)) {
      Alert.alert(
        'Invalid phone number',
        'Please check again your phone number!',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
      );
    } else {
      const updatedUserProfile = {
        fullName: fullName,
        bio: bio,
        email: email,
        phone: phone,
        avatar: avatar,
      };
      const updatedUserProfileString = JSON.stringify(updatedUserProfile);
      UserProfileStorage.storeUserProfileData(updatedUserProfileString);
      // props.route.params.onSelect();
      props.navigation.navigate(SCREEN.USER_PROFILE);
    }
  };

  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'External Storage Write Permission',
            message: 'App needs write permission',
            buttonPositive: 'OK',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        Alert.alert('Write permission err', err);
      }
      return false;
    } else {
      return true;
    }
  };

  const choosePhotoFromLibrary = async (type: string) => {
    let isStoragePermitted = await requestExternalWritePermission();
    let options = {
      mediaType: type,
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
    };
    if (isStoragePermitted) {
      launchImageLibrary(options, response => {
        if (response.didCancel) {
          Alert.alert('User cancelled camera picker');
          return;
        } else if (response.errorCode === 'camera_unavailable') {
          Alert.alert('Camera not available on device');
          return;
        } else if (response.errorCode === 'permission') {
          Alert.alert('Permission not satisfied');
          return;
        } else if (response.errorCode === 'others') {
          Alert.alert(response.errorMessage);
          return;
        }
        setAvatar(response.uri);
      });
    }
  };
  return (
    <View>
      <Header onBackClick={() => props.navigation.goBack()} />
      <View style={styles.container}>
        <View style={styles.firstView}>
          <Text style={styles.editProfileText}>Edit Profile</Text>
          <View style={styles.changeAvatarWrapper}>
            <Image
              source={{uri: avatar}}
              style={{
                width: PixelRatio.getPixelSizeForLayoutSize(50),
                height: PixelRatio.getPixelSizeForLayoutSize(50),
                borderRadius: PixelRatio.getPixelSizeForLayoutSize(25),
              }}
            />
            <TouchableOpacity
              style={{marginTop: PixelRatio.getPixelSizeForLayoutSize(1)}}
              activeOpacity={0.8}>
              <Text
                style={styles.chooseImageText}
                onPress={() => choosePhotoFromLibrary('photo')}>
                Edit Profile Picture
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.secondView}>
          <View>
            <Text style={styles.text}>Full Name</Text>
            <TextInput
              style={styles.textInput}
              value={fullName}
              onChangeText={text => setFullName(text)}
            />
          </View>
          <View>
            <Text style={styles.text}>Bio</Text>
            <TextInput
              style={styles.textInput}
              value={bio}
              onChangeText={text => setBio(text)}
            />
          </View>
        </View>
        <View style={styles.thirdView}>
          <Text style={styles.privateInformationText}>Private Information</Text>
          <View>
            <Text style={styles.text}>Email</Text>
            <TextInput
              style={styles.textInput}
              value={email}
              onChangeText={text => setEmail(text)}
            />
          </View>
          <View>
            <Text style={styles.text}>Phone</Text>
            <TextInput
              style={styles.textInput}
              value={phone}
              onChangeText={text => setPhone(text)}
            />
          </View>
        </View>
        <View style={styles.footerView}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.button}
            onPress={onSaveProfileHandle}>
            <Text style={styles.saveProfileText}>Save Profile</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    color: '#A8A8A8',
  },
  textInput: {
    borderBottomWidth: 1,
    borderBottomColor: '#CCCCCC',
    height: Scale.scale(35),
    paddingVertical: Scale.scale(5),
  },
  button: {
    backgroundColor: Colors.primary,
    height: Scale.scale(50),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Scale.scale(10),
  },
  saveProfileText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  chooseImageText: {
    color: Colors.primary,
    fontWeight: 'bold',
  },
  changeAvatarWrapper: {
    alignItems: 'center',
    marginTop: PixelRatio.getPixelSizeForLayoutSize(5),
  },
  footerView: {
    flex: 0.12,
  },
  privateInformationText: {
    fontSize: Scale.scale(16),
    fontWeight: 'bold',
  },
  container: {
    padding: Scale.scale(10),
    flex: 1,
  },
  firstView: {
    flex: 0.32,
  },
  editProfileText: {
    fontSize: Scale.scale(25),
    fontWeight: 'bold',
  },
  secondView: {
    flex: 0.28,
  },
  thirdView: {
    flex: 0.28,
  },
});
