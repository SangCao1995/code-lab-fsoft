import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Alert,
  ImageSourcePropType,
} from 'react-native';
import {recipeData} from '../../constants';
import {RecipteCard} from './components';
import {Header} from '../../components';
import {RecipesStorage} from '../../utils';
import {useDispatch, useSelector} from 'react-redux';
import {recipeActions} from '../../store/actions';

interface Recipe {
  id: string;
  title: string;
  image: ImageSourcePropType;
}

export const RecipeFeedScreen: React.FC = () => {
  const [recipe, setRecipe] = useState(recipeData);
  // const [recipesSaved, setRecipesSaved] = useState([]);
  const dispatch = useDispatch();
  const user = useSelector(state => state.auth.user);
  // const recipesSaved: [] = [];
  const onIsLikedHandle = (item: Recipe) => {
    setRecipe(currentRecipes =>
      currentRecipes.map(currentRecipe => {
        if (currentRecipe.id === item.id) {
          currentRecipe.isLiked = !currentRecipe.isLiked;
        }
        return currentRecipe;
      }),
    );
  };

  const onSaveHandle = (item: Recipe) => {
    // const recipes = [];
    // recipesSaved.push(item);

    // const recipesData = JSON.stringify(recipesSaved);

    // RecipesStorage.storeRecipesData(recipesData);
    const data = {
      userId: user.id,
      recipeId: item.id,
      title: item.title,
      image: item.image,
    };
    dispatch(
      recipeActions.saveRecipe(data, recipesData => {
        const recipesDataSaved = JSON.stringify(recipesData);
        RecipesStorage.storeRecipesData(recipesDataSaved);
      }),
    );
    Alert.alert('Save', 'Already saved item', [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {text: 'OK', onPress: () => console.log('OK Pressed')},
    ]);
  };

  return (
    <View style={styles.container}>
      <Header isNotice />
      <View style={{alignItems: 'center'}}>
        <FlatList
          keyExtractor={(item: Recipe, index: number) => item.id}
          data={recipe}
          renderItem={({item}) => (
            <RecipteCard
              key={item.id}
              data={item}
              onIsLiked={() => onIsLikedHandle(item)}
              onSaveClick={() => onSaveHandle(item)}
              goToRecipeDetail={() => {}}
            />
          )}
          ItemSeparatorComponent={() => <View style={{height: 15}} />}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});
