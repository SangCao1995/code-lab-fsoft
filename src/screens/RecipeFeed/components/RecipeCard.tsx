import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  ImageSourcePropType,
} from 'react-native';
import {Images} from '../../../images';
import {Colors} from '../../../themes';
import {Scale} from '../../../utils';

interface Recipe {
  id: string;
  title: string;
  description: string;
  likedAmount: number;
  commentedAmount: number;
  image: ImageSourcePropType;
  avatar: ImageSourcePropType;
  isLiked: boolean;
  datedPost: number;
  profileName: string;
}

interface Props {
  data: Recipe;
  onIsLiked: () => void;
  onSaveClick: () => void;
  goToRecipeDetail: () => void;
}

export const RecipteCard: React.FC<Props> = ({
  data,
  onIsLiked,
  onSaveClick,
  goToRecipeDetail,
}) => {
  return (
    <TouchableOpacity style={styles.card} onPress={goToRecipeDetail}>
      <Image
        source={data.image}
        style={{width: Scale.scale(325), height: Scale.scale(200)}}
      />
      <View style={styles.details}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={{fontSize: 16, fontWeight: 'bold'}}>{data.title}</Text>
          <TouchableOpacity activeOpacity={0.8} onPress={onIsLiked}>
            <Image
              source={
                data.isLiked ? Images.img_is_liked : Images.img_is_unLiked
              }
              style={styles.likedImage}
            />
          </TouchableOpacity>
        </View>
        <Text style={{fontSize: 11, color: Colors.gray}}>
          {data.description}
        </Text>
        <View style={styles.lastRowWrapper}>
          <View style={styles.lastRowLeft}>
            <Text style={{fontSize: 13}}>{data.likedAmount} likes</Text>
            <Text>•</Text>
            <Text style={{fontSize: 13}}>{data.commentedAmount} Commnents</Text>
          </View>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.saveButton}
            onPress={onSaveClick}>
            <Image source={Images.img_add} style={styles.addImage} />
            <Text style={{fontSize: 13, color: Colors.primary}}>Save</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.profileWrapper}>
        <Image source={Images.img_avatar} />
        <View style={{marginLeft: 10}}>
          <Text style={{fontSize: 13}}>{data.profileName}</Text>
          <Text style={{fontSize: 11, color: Colors.gray}}>
            {data.datedPost}h ago
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  card: {
    height: 360,
    borderRadius: 10,
    overflow: 'hidden',
    marginHorizontal: Scale.scale(25),
    width: Scale.scale(325),
  },
  likedImage: {
    width: 24,
    height: 24,
    marginRight: 5,
  },
  lastRowWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  saveButton: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 60,
    height: 26,
    borderWidth: 1,
    borderColor: Colors.primary,
    justifyContent: 'center',
    borderRadius: 5,
    paddingRight: 3,
  },
  addImage: {
    width: 14,
    height: 14,
    marginRight: 8,
  },
  profileWrapper: {
    height: 62,
    backgroundColor: 'white',
    position: 'absolute',
    flexDirection: 'row',
    width: '100%',
    opacity: 0.9,
    alignItems: 'center',
    paddingHorizontal: 15,
  },
  lastRowLeft: {
    flexDirection: 'row',
    width: 140,
    justifyContent: 'space-between',
  },
  details: {
    justifyContent: 'space-around',
    height: 160,
    paddingVertical: 10,
    paddingHorizontal: 16,
  },
});
