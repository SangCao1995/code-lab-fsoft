import AsyncStorage from '@react-native-community/async-storage';

export const storeRecipesData = async (recipes: string) => {
  try {
    await AsyncStorage.setItem('@recipes', recipes);
  } catch (error) {
    console.log(error);
  }
};

export const retrieveRecipeData = async () => {
  try {
    const recipe = await AsyncStorage.getItem('@recipes');
    if (recipe !== null) {
      return recipe;
    }
    return '';
  } catch (error) {
    console.log(error);
  }
};
