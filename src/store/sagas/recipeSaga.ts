import {ImageSourcePropType} from 'react-native';
import {call, put, takeLatest} from 'redux-saga/effects';
import {recipesType} from '../actions/actionTypes';

interface RecipeSave {
  userId: string;
  recipeId: string;
  title: string;
  image: ImageSourcePropType;
}

function* saveRecipe(recipe: RecipeSave) {
  const recipePromise: Promise<RecipeSave> = yield new Promise(resolve => {
    resolve(recipe);
  });
  return recipePromise;
}

interface recipeSaveAction {
  data: RecipeSave;
  resolve: (arg0: RecipeSave) => void;
}

function* fetchSaveRecipe(action: recipeSaveAction) {
  try {
    const response: RecipeSave = yield call(saveRecipe, action.data);
    yield put({type: recipesType.SAVE_RECIPE_SUCCESS, data: response});
    action.resolve && action.resolve(response);
  } catch (error) {
    console.log(error);
  }
}

export function* recipesWatcher() {
  // @ts-ignore
  yield takeLatest(recipesType.SAVE_RECIPE, fetchSaveRecipe);
}
