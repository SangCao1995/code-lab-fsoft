import {fork, all} from 'redux-saga/effects';
import {authWatcher} from './authSaga';
import {recipesWatcher} from './recipeSaga';

export default function* rootSaga() {
  yield all([fork(authWatcher), fork(recipesWatcher)]);
}
