import {select, call, put, takeLatest} from 'redux-saga/effects';
import {UserProfile} from '../../constants';
import {authActions} from '../actions';
import {authTypes} from '../actions/actionTypes';

function* checkUserLogin(userInfo: UserProfile) {
  const users: Array<UserProfile> = yield select(state => state.auth.users);
  console.log(users);
  return users.some(
    (user: UserProfile) =>
      user.userName === userInfo.userName &&
      user.password === userInfo.password,
  );
}

function* login(userInfo: UserProfile) {
  const users: Array<UserProfile> = yield select(state => state.auth.users);
  const loginPromise: Promise<UserProfile | string> = yield new Promise(
    (resolve, reject) => {
      setTimeout(() => {
        if (checkUserLogin(userInfo)) {
          resolve(
            users.find(
              (user: UserProfile) =>
                user.userName === userInfo.userName &&
                user.password === userInfo.password,
            ),
          );
        } else {
          reject('User is not found');
        }
      }, 2000);
    },
  );
  return loginPromise;
}

function* logout() {
  const logoutPromise: Promise<UserProfile | string> = yield new Promise(
    resolve => {
      resolve({});
    },
  );
  return logoutPromise;
}

interface fetchLoginAction {
  data: UserProfile;
  resolve: (arg0: UserProfile) => void;
}

function* fetchLogin(action: fetchLoginAction) {
  try {
    // yield take(authTypes.LOGIN_PENDING);
    yield put(authActions.loginPending());
    const response: UserProfile = yield call(login, action.data);

    console.log(response);
    yield put({type: authTypes.LOGIN_SUCCESS, data: response});
    action.resolve && action.resolve(response);
  } catch (error) {
    console.log(error);
  }
}

interface fetchLogoutAction {
  data: UserProfile;
  resolve: ({}) => void;
}

function* fetchLogout(action: fetchLogoutAction) {
  try {
    const response: {} = yield call(logout);
    yield put({type: authTypes.LOGOUT_SUCCESS, data: response});
    action.resolve && action.resolve(response);
  } catch (error) {
    console.log(error);
  }
}

export function* authWatcher() {
  // @ts-ignore
  yield takeLatest(authTypes.LOGIN, fetchLogin);
  // @ts-ignore
  yield takeLatest(authTypes.LOGOUT, fetchLogout);
}
