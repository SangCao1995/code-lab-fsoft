import {recipesType} from '../actions/actionTypes';

const initialState = {
  recipesSaved: [],
};

const recipesReducer = (state = initialState, action) => {
  switch (action.type) {
    case recipesType.SAVE_RECIPE_SUCCESS:
      const recipesSaved = [...state.recipesSaved, action.data];
      return {...state, recipesSaved};
  }
  return state;
};

export default recipesReducer;
