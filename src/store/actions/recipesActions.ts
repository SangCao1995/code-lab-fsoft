import {recipesType} from './actionTypes';
import {ImageSourcePropType} from 'react-native';

export const recipeActions = {
  saveRecipe,
};

interface RecipeSave {
  userId: string;
  recipeId: string;
  title: string;
  image: ImageSourcePropType;
}

function saveRecipe(data: RecipeSave, resolve: () => void) {
  return {
    type: recipesType.SAVE_RECIPE,
    data,
    resolve,
  };
}
