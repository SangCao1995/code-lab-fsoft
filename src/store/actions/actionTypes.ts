interface authTypesInterface {
  LOGIN_PENDING: string;
  LOGIN: string;
  LOGIN_SUCCESS: string;
  LOGOUT: string;
  LOGOUT_SUCCESS: string;
}

interface recipesTypeInterface {
  SAVE_RECIPE: string;
  SAVE_RECIPE_SUCCESS: string;
  EDIT_RECIPE: string;
  EDIT_RECIPE_SUCCESS: string;
}

export const authTypes: authTypesInterface = {
  LOGIN_PENDING: 'LOGIN_PENDING',
  LOGIN: 'LOGIN',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  LOGOUT: 'LOGOUT',
  LOGOUT_SUCCESS: 'LOGOUT_SUCCESS',
};

export const recipesType: recipesTypeInterface = {
  SAVE_RECIPE: 'SAVE_RECIPE',
  SAVE_RECIPE_SUCCESS: 'SAVE_RECIPE_SUCCESS',
  EDIT_RECIPE: 'EDIT_RECIPE',
  EDIT_RECIPE_SUCCESS: 'EDIT_RECIPE_SUCCESS',
};
