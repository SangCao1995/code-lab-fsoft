import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {SCREEN} from './Screen';
import {TabNavigator} from './TabNavigator';
import {LoginScreen, ProfileEdittingScreen} from '../screens';
import {RootStateOrAny, useSelector} from 'react-redux';
import {UserProfile} from '../constants';

const Stack = createStackNavigator();

export const StackNavigator = () => {
  const user: UserProfile = useSelector(
    (state: RootStateOrAny) => state.auth.user,
  );
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      {Object.keys(user).length > 0 ? (
        <Stack.Screen name={SCREEN.TAB} component={TabNavigator} />
      ) : (
        <Stack.Screen name={SCREEN.LOGIN} component={LoginScreen} />
      )}
      <Stack.Screen
        name={SCREEN.PROFILE_EDITING}
        component={ProfileEdittingScreen}
      />
    </Stack.Navigator>
  );
};
