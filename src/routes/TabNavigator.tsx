import React from 'react';
import {Image} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {SCREEN} from './Screen';
import {
  UserProfileScreen,
  SearchScreen,
  RecipeFeedScreen,
  RecipeEditScreen,
} from '../screens';
import {Colors} from '../themes';
import {Images} from '../images';

const Tab = createBottomTabNavigator();

const screenOptions = ({route}) => ({
  tabBarIcon: ({focused, color, size}) => {
    if (route.name === SCREEN.USER_PROFILE) {
      return (
        <Image
          source={focused ? Images.img_recipe_active : Images.img_recipe}
        />
      );
    } else if (route.name === SCREEN.RECIPE_FEED) {
      return (
        <Image source={focused ? Images.img_feed_active : Images.img_feed} />
      );
    } else {
      return (
        <Image
          source={
            focused ? Images.img_search_active : Images.img_search_unactive
          }
        />
      );
    }
  },
});

const tabBarOptions = {
  activeTintColor: Colors.primary,
  inactiveTintColor: 'gray',
  showLabel: false,
};

export const TabNavigator = props => {
  return (
    <Tab.Navigator
      {...props}
      screenOptions={screenOptions}
      tabBarOptions={tabBarOptions}
      initialRouteName={SCREEN.RECIPE_EDIT}>
      <Tab.Screen name={SCREEN.RECIPE_EDIT} component={RecipeEditScreen} />
      <Tab.Screen name={SCREEN.RECIPE_FEED} component={RecipeFeedScreen} />
      <Tab.Screen name={SCREEN.USER_PROFILE} component={UserProfileScreen} />
    </Tab.Navigator>
  );
};
